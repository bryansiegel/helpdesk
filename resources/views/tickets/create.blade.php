@extends('layouts.app')

@section('content')
<form method="post" action="{{ route('tickets.store') }}" >
	@csrf
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Ticket</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

					<form method="post" action="{{ route('tickets.store') }}" >
						@csrf
						<div class="form-group">
					    	<label for="priority">Priority</label>
					    	<select name="priority" class="form-control">
					    		<option>Select A Priority</option>
					    		<option value="low">Low</option>
					    		<option value="high">High</option>
					    	</select>
					    </div>
					    <div class="form-group">
					    	<label for="subject">Subject</label>
					        <input type="text" name="subject" class="form-control" placeholder="Subject"/>
					    </div>
					    <div class="form-group">
					    	<label for="description">Description</label>
					        <textarea name="description" class="form-control" placeholder="Description" cols="6" rows="6"></textarea>
					    </div>
					    <div>
					    <a href="/tickets/" class="btn btn-secondary">Cancel</a>
					    <button class="btn btn-primary">Create</button>    
					</form>                
				</div>
            </div>
        </div>
    </div>
</div>
@endsection