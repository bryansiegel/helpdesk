@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Ticket</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

					<form method="post" action="/tickets/{{ $ticket->id }}" >
						@csrf
						{{ method_field('PATCH') }}
						<div class="form-group">
					    	<label for="priority">Priority</label>
 					    	<select name="priority" class="form-control">
					    	@foreach($priority_options as $key => $value)
								<option value="{{ $key }}" {{ $key == $ticket->priority ? 'selected' : ''}} value="{{ $ticket->priority }}">{{ $value }}</option>
							@endforeach	
					    	</select>
					    </div>
					    <div class="form-group">
					    	<label for="status">Status</label>
					    	<select name="status" class="form-control">
					    		@foreach($status_options as $key => $value)
					    			<option value="{{ $key }}" {{ $key == $ticket->status ? 'selected' : '' }} value="{{ $ticket->status }}"> {{ $value }}</option>
					    		@endforeach
					    	</select>
					    </div>
					    <div class="form-group">
					    	<label for="subject">Subject</label>
					        <input type="text" name="subject" class="form-control" value="{{ $ticket->subject }}" placeholder="Subject"/>
					    </div>
					    <div class="form-group">
					    	<label for="description">Description</label>
					        <textarea name="description" class="form-control" value="{{ $ticket->description }}" placeholder="Description" cols="6" rows="6">{{ $ticket->description }}</textarea>
					    </div>
					    <div>
					    <a href="/tickets/" class="btn btn-secondary">Cancel</a>
					    <button class="btn btn-primary">Update</button>    
					</form>                
				</div>
            </div>
        </div>
    </div>
</div>


@endsection