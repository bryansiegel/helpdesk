@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tickets <a href="/tickets/create/" class="btn btn-primary float-right">Create</a></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   <table class="table table-stripped">
                   	<thead>
                   		<tr>
                   			<td>Priority</td>
                   			<td>Subject</td>
                   			<td>Description</td>
                   			<td>Status</td>
                   			<td></td>
                   		</tr>
                   	</thead>
                   	<tbody>
                   @foreach($tickets as $ticket)
                   <tr>
                   	<td>{{ $ticket->priority }}</td>
                   	<td>{{ $ticket->subject }}</td>
                   	<td>{{ $ticket->description }}</td>
                   	<td>{{ $ticket->status }}</td>
                   	<td class="btn-group">
                   		<a href="/tickets/{{ $ticket->id }}" class="btn btn-primary">View</a>
                   		<a href="/tickets/{{ $ticket->id }}/edit/" class="btn btn-success">Edit</a>
                   		<form method="post" action="{{ route('tickets.destroy', $ticket->id) }}">
                   			@csrf
                   			@method('DELETE')
                   		<button class="btn btn-danger" type="submit">Delete</button>
                   		</form> 
                   	</td>
                   	</tr>
                   @endforeach
                   	</tbody>

                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection