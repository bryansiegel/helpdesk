@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Show Ticket</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

					<ul class="list-group">
						<li class="list-group-item"><strong>Priority:</strong> {{ $ticket->priority }}</li>
						<li class="list-group-item"><strong>Subject:</strong> {{ $ticket->subject }}</li>
						<li class="list-group-item"><strong>Description:</strong> {{ $ticket->description }}</li>
						<li class="list-group-item"><strong>Status:</strong> {{ $ticket->status }}</li>
					</ul>
					<br>
					<a href="/tickets" class="btn btn-danger"><< Back</a>       
				</div>
            </div>
        </div>
    </div>
</div>
@endsection