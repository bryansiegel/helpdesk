<?php

namespace App\Http\Controllers;
use App\Models\Ticket;

use Illuminate\Http\Request;

class TicketsController extends Controller
{
    public function index()
    {
        $tickets = Ticket::all();
        
        return view('tickets.index', compact('tickets'));
    }

    public function create()
    {
        return view('tickets.create');

    }

    public function store(Request $request)
    {
        $request->validate([
            'subject' => 'required',
            // 'note' => 'required'
            'priority' => 'required'
        ]);

        $ticket = new Ticket([
            'priority' => $request->get('priority'),
            'subject' => $request->get('subject'),
            'group' => $request->get('group'),
            'requester' => $request->get('requester'),
            'description' => $request->get('description'),
            'status' => 'open',
        ]);
        $ticket->save();
        return redirect('/tickets')->with('success', 'Ticket was created');
    }

    public function show($id)
    {
        $ticket = Ticket::find($id);

        return view('tickets.show', compact('ticket'));
    }

    public function edit($id)
    {
        $ticket = Ticket::find($id);
        $priority_options = ["low" => "Low", "high" => "High"];
        $status_options = ["open" => "Open", "in_progress" => "In Progress", "closed" => "Closed"];

        return view('tickets.edit', compact( ['ticket', 'priority_options', 'status_options'] ));

    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'subject' => 'required',
            // 'note' => 'required'
        ]);

        $ticket = Ticket::find($id);
        $ticket->subject = $request->get('subject');
        $ticket->group = $request->get('group');
        $ticket->requester = $request->get('requester');
        // $request->validate([
        //     'title' => 'required',
        //     'note' => 'required'
        // ]);

        $ticket = Ticket::find($id);
        $ticket->priority = $request->get('priority');
        $ticket->description = $request->get('description');
        $ticket->status = $request->get('status');
        $ticket->save();

        return redirect('/tickets')->with('success', 'Ticket Has Been Updated');
    }

    public function destroy($id)
    {
        $ticket = Ticket::destroy($id);

        return redirect('/tickets')->with('success', 'Ticket Has Been Destroyed');
    }
}
