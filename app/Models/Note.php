<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
    	'note',
    ];

    //Tickets
    public function ticket()
    {
    	return $this->belongsTo('App\Models\Ticket');
    }
}
