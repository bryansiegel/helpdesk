<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'group',
        'requestor',
        'priority',
        'subject',
        'description',
        'status'
    ];

    public function notes()
    {
        return $this->hasMany('Ticket');
    }
}
